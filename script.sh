#!/bin/bash

#Exporting the GPIO
echo -n "182" >/sys/class/gpio/export

#Setting Direction of GPIO pin
echo -n "out" >/sys/class/gpio/gpio182/direction

#When TURN ON is pressed
if [ $1 -eq 1 ]; then
	echo -n "1" >/sys/class/gpio/gpio182/value
fi

#When TURN OFF is pressed
if [ $1 -eq 0 ]; then
	echo -n "0" >/sys/class/gpio/gpio182/value
fi